FROM python:2.7-alpine

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ARG version=master

RUN apk --update add nodejs npm && \
    apk --update add --virtual .deps make git && \
    cd /tmp && \
    git clone https://github.com/AnalyticalGraphicsInc/3d-tiles-tools.git 3d-tiles-tools && \
    cd /tmp/3d-tiles-tools && \
    git checkout ${version} && \
    mkdir -p /opt/3d-tiles-tools && \
    cp -r tools /opt/3d-tiles-tools/ && \
    cp -r validator /opt/3d-tiles-tools/ && \
    cd /opt/3d-tiles-tools/tools && \
    npm install && \
    chmod 755 bin/3d-tiles-tools.js && \
    ln -s /opt/3d-tiles-tools/tools/bin/3d-tiles-tools.js /bin/3d-tiles-tools && \
    cd /opt/3d-tiles-tools/validator && \
    npm install && \
    chmod 755 bin/3d-tiles-validator.js && \
    ln -s /opt/3d-tiles-tools/validator/bin/3d-tiles-validator.js /bin/3d-tiles-validator && \
    apk del .deps && \
    rm -rf /tmp/*

WORKDIR /data

CMD ["/bin/sh"]
