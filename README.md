# Dockerized 3D Tiles Tools

This is a dockerized version of [3D Tiles Tools](https://github.com/AnalyticalGraphicsInc/3d-tiles-tools) for the Cesium Web Globe.

## Usage

```
docker run -it --rm -v /path/to/your/tilesets:/data registry.gitlab.com/gf-bl/docker/3d-tiles-tools:latest 3d-tiles-tools --help
```
